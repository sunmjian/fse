package com.jfirer.fse;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 3)
@Measurement(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Threads(1)
@Fork(2)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
public class BenchMark
{
    Fse       fse   = new Fse();
    Fse       fse_3 = new Fse().useCompile();
    TestData  data  = new TestData();
    ByteArray buf   = ByteArray.allocate(100);

    @Benchmark
    public void testNoCompile()
    {
        buf.clear();
        fse.serialize(data, buf);
    }

    @Benchmark
    public void testDirectCompile()
    {
        buf.clear();
        fse_3.serialize(data, buf);
    }

    public static void main(String[] args) throws RunnerException
    {
        Options opt = new OptionsBuilder().include(BenchMark.class.getSimpleName()).build();
        new Runner(opt).run();
    }
}
