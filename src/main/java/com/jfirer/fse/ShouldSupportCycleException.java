package com.jfirer.fse;

public class ShouldSupportCycleException extends RuntimeException
{
    @Override
    public Throwable fillInStackTrace()
    {
        return this;
    }
}
